import Calculator.Value;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by adam12 on 27.09.17.
 * This Class tested the basic calculation and memory of value.
 */
public class TestValue {
    // main test for object Value Geter and setters
    @Test
    public void value_ISnot_Null() {
        Value value = new Value();
        Assert.assertNotNull(value);
    }
    @Test
    public void value_Set_val1(){
        Value value =new Value();
        value.setVal1("7AS");
        Assert.assertEquals("7AS",value.getVal1());
    }
    @Test
    public void value_Set_val2(){
        Value value =new Value();
        value.setVal2("7AS");
        Assert.assertEquals("7AS",value.getVal2());
    }
    @Test
    public void value_Set_result(){
        Value value =new Value();
        value.setResult(7);
        Assert.assertEquals(7,value.getResult());
    }

    //test for Calculations
    // Additional

    //Integer

    @Test
    public void calculations_Additional_Integer() {
        Value value = new Value();
                Assert.assertEquals(2,value.addition(1,1));
    }
    @Test
    public void calculations_Additional_Integer_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(0,value.addition(-1,1));
    }

    @Test
    public void calculations_Additional_Integer_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(0,value.addition(1,-1));
    }
    @Test
    public void calculations_Additional_Integer_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(-2,value.addition(-1,-1));
    }

    // Decimal
    @Test
    public void calculations_Additiona_lDecimal_Number() {
        Value value = new Value();
        Assert.assertEquals(2.2f,value.addition(1.1f,1.1f));
    }

    @Test
    public void calculations_Additional_Decimal_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1.1f+1.0f,value.addition(-1.1f,1.0f));
    }

    @Test
    public void calculations_Additional_Decimal_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(0.0f,value.addition(1.1f,-1.1f));
    }
    @Test
    public void calculations_Additional_decimal_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(-2.2f,value.addition(-1.1f,-1.1f));
    }
//test for Calculations
    // Subtraction

    //Integer

    @Test
    public void calculations_Subtraction_Integer() {
        Value value = new Value();
        Assert.assertEquals(0,value.subtraction(1,1));
    }
    @Test
    public void calculations_Subtraction_Integer_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-2,value.subtraction(-1,1));
    }

    @Test
    public void calculations_Subtraction_Integer_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(2,value.subtraction(1,-1));
    }
    @Test
    public void calculations_Subtraction_Integer_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(0,value.subtraction(-1,-1));
    }

    // Decimal
    @Test
    public void calculations_Subtraction_Decimal_Number() {
        Value value = new Value();
        Assert.assertEquals(0.0f,value.subtraction(1.1f,1.1f));
    }

    @Test
    public void calculations_Subtraction_Decimal_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1.1f-(1.0f),value.subtraction(-1.1f,1.0f));
    }

    @Test
    public void calculations_Subtraction_Decimal_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(2.2f,value.subtraction(1.1f,-1.1f));
    }
    @Test
    public void calculations_Subtraction_decimal_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(0.0f,value.subtraction(-1.1f,-1.1f));
    }

//test for Calculations
    // multiplication

    //Integer

    @Test
    public void calculations_multiplication_Integer() {
        Value value = new Value();
        Assert.assertEquals(1,value.multiplication(1,1));
    }
    @Test
    public void calculations_multiplication_Integer_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1,value.multiplication(-1,1));
    }

    @Test
    public void calculations_multiplication_Integer_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(-1,value.multiplication(1,-1));
    }
    @Test
    public void calculations_multiplication_Integer_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(1,value.multiplication(-1,-1));
    }

    // Decimal
    @Test
    public void calculations_multiplication_Decimal_Number() {
        Value value = new Value();
        Assert.assertEquals(1.1f*1.1f,value.multiplication(1.1f,1.1f));
    }

    @Test
    public void calculations_multiplication_Decimal_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1.1f*(1.0f),value.multiplication(-1.1f,1.0f));
    }

    @Test
    public void calculations_multiplication_Decimal_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(1.1f*(-1.1f),value.multiplication(1.1f,-1.1f));
    }
    @Test
    public void calculations_multiclication_decimal_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(-1.1f*(-1.1f),value.multiplication(-1.1f,-1.1f));
    }

    //test for Calculations
    // division

    //Integer

    @Test
    public void calculations_division_Integer() {
        Value value = new Value();
        Assert.assertEquals(1,value.division(1,1));
    }
    @Test
    public void calculations_division_Integer_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1,value.division(-1,1));
    }

    @Test
    public void calculations_division_Integer_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(-1,value.division(1,-1));
    }
    @Test
    public void calculations_division_Integer_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(1,value.division(-1,-1));
    }

    // Decimal
    @Test
    public void calculations_division_Decimal_Number() {
        Value value = new Value();
        Assert.assertEquals(1.1f/1.1f,value.division(1.1f,1.1f));
    }

    @Test
    public void calculations_division_Decimal_Minus_val1() {
        Value value = new Value();
        Assert.assertEquals(-1.1f/(1.0f),value.division(-1.1f,1.0f));
    }

    @Test
    public void calculations_division_Decimal_Minus_val2() {
        Value value = new Value();
        Assert.assertEquals(1.1f/(-1.1f),value.division(1.1f,-1.1f));
    }
    @Test
    public void calculations_division_decimal_Minus_val1_andVal2() {
        Value value = new Value();
        Assert.assertEquals(-1.1f/(-1.1f),value.division(-1.1f,-1.1f));
    }

    /*  Test Convertion function
*
 */
    @Test
    public void convert_String_to_Float_case_DOT(){
        Value value = new Value();
        Assert.assertEquals(3.0f,value.contvertNumber("3,00"));
    }
    @Test
    public void convert_String_to_Float_case_Coma(){
        Value value = new Value();
        Assert.assertEquals(3.0f,value.contvertNumber("3,00"));
    }

    @Test
    public void Convert_To_String_Case_dot_to_coma(){
        Value value = new Value();
        Float f = 0.0f;
        Assert.assertEquals("0,0",value.convertToSting(f));
    }


}