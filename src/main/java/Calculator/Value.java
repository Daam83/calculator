package Calculator;

/**
 * Created by adam12 on 21.09.17.
 * This Class are container for Value nothing else.
 */
public class Value extends Calculations {
    private String calculations = "";

    private String val1 = "";
    private String val2 = "";
    private Number result = 0;

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }


    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }


    public Number getResult() {
        return result;
    }

    public void setResult(Number result) {
        this.result = result;
    }

    public String getCalculations() {
        return calculations;
    }

    public void setCalculations(String calculations) {
        this.calculations = calculations;
    }


}
