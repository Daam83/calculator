package Calculator;
/**
 * Created by adam12 on 01.10.17.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StartCalculator extends Application {

    /**
     * This Class are the start class for Calculator. they run the GUI created from fxml file.
     * Gui works but without test. Author are on study framework for tested JavaFX.
     */

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Calculator 0.1");

        Parent layout = FXMLLoader.load(getClass().getResource("DaamCalc01.fxml"));

        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
