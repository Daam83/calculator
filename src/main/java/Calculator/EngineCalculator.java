package Calculator;


import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Created by adam12 on 01.10.17.
 * This class are Engine of Calculator. It bridge between GUI and function classes.
 */
public class EngineCalculator {
    Value value = new Value();

    @FXML
    private Label display_symbol;

    @FXML
    private Label display_values;

    @FXML
    private TextField value_1;
    @FXML
    private TextField value_2;


    @FXML
    public void buttonPlus() {
        /**
         * This method run the addiction of 2 value_1 and value_2.
         * Logic of this method is to set symbol + as Calculation symbol and run method run(). Argument which are
         * sent to this method is FALSE
         * This method run when user preset on "+" button at GUI
         */
        value.setCalculations("+");
        run(false);

    }

    @FXML
    public void buttonMinus() {
        /**
         * This method run the subtraction of 2 value_1 and value_2.
         * Logic of this method is to set symbol - as Calculation symbol and run method run(). Argument which are
         * sent to this method is FALSE
         * This method run when user preset on "-" button at GUI
         */
        value.setCalculations("-");
        run(false);
    }

    @FXML
    public void buttonMultiplication() {
        /**
         * This method run the multiplication of 2 value_1 and value_2.
         * Logic of this method is to set symbol * as Calculation symbol and run method run(). Argument which are
         * sent to this method is FALSE
         * This method run when user preset on "*" button at GUI
         */
        value.setCalculations("*");
        run(false);
    }

    @FXML

    public void buttonDivision() {
        /**
         * This method run the division of 2 value_1 and value_2.
         * Logic of this method is to set symbol / as Calculation symbol and run method run(). Argument which are
         * sent to this method is FALSE
         * This method run when user preset on "/" button at GUI
         */
        value.setCalculations("/");
        run(false);
    }

    @FXML
    public void buttonClear() {
        /**
         * This method run clear process.
         * Logic of this method is to set symbol Reset as Calculation symbol and run method run(). Argument which are
         * sent to this method is TRUE
         * This method run when user preset on "C" button at GUI
         */
        value.setCalculations("Reset");

        run(true);
    }

    public void run(boolean reset) {
        /**
         * This method delete all value or start calculation process.
         * @param reset are trigger of reset. Type Boolean
         *              TRUE then all value and display_value are set 0.
         *              FALSE set on:
         *              -displayed_symbol symbol of calculations
         *              -transfer from  text filed on GUI to Value
         *              -Execute the calculation which are selected by user.
         *              -display_value result of calculation
         *
         */
        if (reset) {
            setDisplay_symbolText(value.getCalculations());
            setValue_1("0");
            setValue_2TEXT("0");
            value.setVal1(getValue_1Text());
            value.setVal2(getValue_2TEXT());
            execute(value.getVal1(), value.getVal2(), value.getCalculations());
            setDisplay_values(value.convertToSting(value.getResult()));


        } else {
            setDisplay_symbolText(value.getCalculations());
            value.setVal1(getValue_1Text());
            value.setVal2(getValue_2TEXT());
            execute(value.getVal1(), value.getVal2(), value.getCalculations());
            setDisplay_values(value.convertToSting(value.getResult()));

        }


    }

     /*
    * Method:
     * -setDisplay_symbolText
      * -setDisplay_values
      * -setValue_1
      * -setValue_2TEXT
      * set String Argument to Text on appropriate fields in GUI DaamCalc01.fxml
     */

    void setValue_1(String value_1) {
        this.value_1.setText(value_1);
    }

    String getDisplay_symbolText() {
        return display_symbol.getText();
    } // use for test

    void setDisplay_symbolText(String setText) {
        this.display_symbol.setText(setText);
    }

    String getDisplay_values() {
        return display_values.getText();
    } // use for test

     /*
    * Methods:
     * -getDisplay_symbolText
      * -getDisplay_values
      * -getValue_1
      * -getValue_2TEXT
      * get String Argument to Text on appropriate fields in GUI DaamCalc01.fxml
     */

    void setDisplay_values(String setText) {
        this.display_values.setText(setText);
    }

    String getValue_1Text() {
        return value_1.getText();
    }

    String getValue_2TEXT() {
        return value_2.getText();
    }

    void setValue_2TEXT(String value_2) {
        this.value_2.setText(value_2);
    }

    public void execute(String val1, String val2, String calculations) {
        /**
         * EXECUTE method choose with arithmetic calculation are set call right calculation for sent parameters Val_1 and
         * val_2 after converted this String to Number.
         *  @param  val1 - String wich are from the first value
         *  @param  val2 - String wich are from the second value
         *  @param  calculations - String wich are from the calculation value
         *
         *  this is method don't return any result but result are set to Value.setResult method.
         */


        if (calculations.equalsIgnoreCase("+")) {
            value.setResult(value.addition(value.contvertNumber(val1), value.contvertNumber(val2)));
        } else if (calculations.equalsIgnoreCase("-")) {
            value.setResult(value.subtraction(value.contvertNumber(val1), value.contvertNumber(val2)));
        } else if (calculations.equalsIgnoreCase("*")) {
            value.setResult(value.multiplication(value.contvertNumber(val1), value.contvertNumber(val2)));
        } else if (calculations.equalsIgnoreCase("/")) {
            value.setResult(value.division(value.contvertNumber(val1), value.contvertNumber(val2)));
        } else if (calculations.equalsIgnoreCase("Reset")) {
            value.setResult(0);
        } else {
            display_symbol.setText("error 404 on execute IF");

        }


    }
}
