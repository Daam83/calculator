package Calculator;

/**
 * Created by adam12 on 21.09.17.
 * This abstract class give method in which are arithmetic calculations basic like:
 * Addition, Subtraction, Multiplication, Division,
 * Additional this Class give method which convert String to Number and Number to String.
 * The convert method contain a logic which:
 * - convert "," to "." and revert.
 */
public abstract class Calculations<T extends Number> {


    public Number addition(T val1, T val2) {
        /**
         * Method return result of arithmetic additional as object witch are extend  by Number
         * @param Number is a generic but to this method will be transfer object wich extends by Number like Float and
         *              Integer.
         *
         * @return a result of addition in type Integer or Float. Type of return depending of combinations input type
         *
         */

        if (val1 instanceof Integer && val2 instanceof Integer) {
            Integer a = ((Number) val1).intValue() + ((Number) val2).intValue();
            return a;
        } else if (val1 instanceof Float && val2 instanceof Float) {
            Float a = ((Number) val1).floatValue() + ((Number) val2).floatValue();
            return a;
        } else if ((val1 instanceof Integer && val2 instanceof Float) || (val1 instanceof Float && val2 instanceof Integer)) {
            Float a = ((Number) val1).floatValue() + ((Number) val2).floatValue();
            return a;
        }

        return null;

    }

    public Number subtraction(T val1, T val2) {
        /**
         * Method return result of arithmetic subtraction as object witch are extend by type Number
         * @param Number is a generic but to this method will be transfer object wich extends by Number like Float and
         *              Integer.
         *
         * @return result of subtraction in type Integer or Float. Type of return depending of combinations input type
         *
         */

        if (val1 instanceof Integer && val2 instanceof Integer) {
            Integer a = ((Number) val1).intValue() - ((Number) val2).intValue();
            return a;
        } else if (val1 instanceof Float && val2 instanceof Float) {
            Float a = ((Number) val1).floatValue() - ((Number) val2).floatValue();
            return a;
        } else if ((val1 instanceof Integer && val2 instanceof Float) || (val1 instanceof Float && val2 instanceof Integer)) {
            Float a = ((Number) val1).floatValue() - ((Number) val2).floatValue();
            return a;
        }

        return null;

    }

    public Number multiplication(T val1, T val2) {
        /**
         * Method return result of arithmetic multiplication as object witch are extend by type Number
         * @param Number is a generic but to this method will be transfer object wich extends by Number like Float and
         *              Integer.
         *
         * @return result of multiplication in type Integer or Float. Type of return depending of combinations input
         * type
         *
         */

        if (val1 instanceof Integer && val2 instanceof Integer) {
            Integer a = ((Number) val1).intValue() * ((Number) val2).intValue();
            return a;
        } else if (val1 instanceof Float && val2 instanceof Float) {
            Float a = ((Number) val1).floatValue() * ((Number) val2).floatValue();
            return a;
        } else if ((val1 instanceof Integer && val2 instanceof Float) || (val1 instanceof Float && val2 instanceof Integer)) {
            Float a = ((Number) val1).floatValue() * ((Number) val2).floatValue();
            return a;
        }

        return null;

    }

    public Number division(T val1, T val2) {
        /**
         * Method return result of arithmetic division as object witch are extend by type Number
         * Additional if val2 are 0 then function return 0
         *
         * @param Number is a generic but to this method will be transfer object wich extends by Number like Float and
         *              Integer.
         *
         * @return result of division in type Integer or Float. Type of return depending of combinations input type
         *
         */

        if (((Number) val2).intValue() == 0) {
            return 0;
        } else if (val1 instanceof Integer && val2 instanceof Integer) {
            Integer a = ((Number) val1).intValue() / ((Number) val2).intValue();
            if (((Number) val1).intValue() % ((Number) val2).intValue() != 0)
                return ((Number) val1).floatValue() / ((Number) val2).intValue();
            else {
                return a;
            }
        } else if (val1 instanceof Float && val2 instanceof Float) {
            Float a = ((Number) val1).floatValue() / ((Number) val2).floatValue();
            return a;
        } else if ((val1 instanceof Integer && val2 instanceof Float) || (val1 instanceof Float && val2 instanceof
                Integer)) {
            Float a = ((Number) val1).floatValue() / ((Number) val2).floatValue();
            return a;
        }

        return null;
    }

    public Number contvertNumber(String in) {
        /**
         * Method return result of conversion from String input to Number.
         * Additional if inputed String are "," then function replaced it to "."
         * @param in Inputed String which will be convert to numeric type. If in is decimal sing then convert to Float.
         *           If not convert to Integer
         *
         * @return result of convert. Return Integer or Float. Type of return depending of combinations input type
         *
         */

        if (in.contains(".")) {
            return Float.valueOf(in);
        } else if (in.contains(",")) {

            return Float.valueOf(in.replace(",", "."));
        } else {
            return Integer.valueOf(in);

        }

    }

    public String convertToSting(Number in) {
        /**
         * Method return result of conversion from Number input to String.
         * Additional inputed Number contain "." then function replaced it to ","
         * @param in Inputed Number which will be convert to String. If in contain decimal sing then convert to "."
         *           to ","
         * @return result of convert. Return String.
         */

        return in.toString().replace(".", ",");
    }


}
